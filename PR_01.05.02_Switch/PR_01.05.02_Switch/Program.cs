﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_if
{
    class Program
    {
        static void Main(string[] args)
        {

            //++++++++++++++++++++++++++
            Console.WriteLine("\nSWITCH MEETOD");

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    Console.WriteLine("Täna puhkame");
                    break;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("Täna teeme trenni");
                    goto default;
                case DayOfWeek.Monday:
                case DayOfWeek.Sunday:
                    Console.WriteLine("Täna teeme pannkooki");
                    goto case DayOfWeek.Wednesday;
                case DayOfWeek.Friday:
                    Console.WriteLine("Täna teeme trenni");
                    break;
                default:
                    Console.WriteLine("Täna töötame");
                    break;
            }



            //++++++++++++++++++++++++++
            Console.WriteLine("\nSWITCH MEETOD 2");

            Console.WriteLine("What colour do you see?  ");
            string c = Console.ReadLine().ToLower();
            switch (c)
            {
                case "GREEN":
                case "green":
                    Console.WriteLine("Driver can drive a car");
                    break;
                case "YELLOW":
                case "yellow":
                    Console.WriteLine("Driver has to be ready to stop");
                    break;
                case "RED":
                case "red":
                    Console.WriteLine("Driver has to stop");
                    break;
                default:
                    Console.WriteLine("The color is unknown.");
                    break;
            }

        }
    }
}
