﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {

            Person henn = new Person()  // see sain numbri 0
            {
                Nimi = "henn sarv",
                IK = "35503070211"
            };

            Console.WriteLine($"Hennu nimi on {henn.Nimi}");

            
            Person maris = new Person()
            {
                Nimi = "Maris Sarv"
            };
            henn.Kaasa = maris;
            Console.WriteLine(maris.Kaasa);

            var ml = henn.NewLaps("Mai-Liis");
            Console.WriteLine(ml.Ema);
        }
    }
}
