﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    enum Gender { Female, Male }

    class Person
    {
        List<Person> People = new List<Person>();
        static int jooksevNumber = 0;

        private string _Nimi;
        private string _IK = "";
        private readonly int jrknr = ++jooksevNumber;

        private Person _Kaasa = null;
      //  private Person _Ema; public Person Ema => _Ema;
      //  private Person _Isa; public Person Isa => _Isa;


        public Person Kaasa
        {
            get => _Kaasa;
            set
            {
                this.Kaasa = value;
                value.Kaasa = this;
            }
        }

      
        public Person NewLaps(string nimi)
        {
            Person laps = new Person { Nimi = nimi };
            if (this.Gender == MisOnKlass.Gender.Male)
            {
                laps._Isa = this;
                laps._Ema = this.Kaasa;
            }
            else
            {
                laps._Ema = this;
                laps._Isa = this.Kaasa;
            }

            return laps;
        }

    
        // mis on konstruktor

        // see siin on kahe parameetriga konstruktor
        public Person(string nimi = "nimeta mats", string ik = "") // 3.
        {
            this.IK = ik;
            this.Nimi = nimi;
            //this.jrknr = ++jooksevNumber;
            People.Add(this);
        }

    

        // see siin on parameetriteta konstruktor
        // kui ühtegi konstruktorit pole, siis see tehakse automaatselt (vaikimisi konstruktor)
        // kui on kasvõi 1 konstruktor, siis vaikimisi parameetriteta konstruktorit ei tehta
        //public Person() : this("nimeta mats") {}    // 1. 
        // see konstruktor pöördub teise poole ja annab parameetriks nime


        // see on ühe parameetriga konstruktor
        // mis omakorda kutsub välja kahe parameetriga konstruktori
        //public Person(string nimi) : this(nimi, "") { } // 2.


        public string getIK() { return _IK; }
        public void setIK(string uusIK)
        {
            if (_IK == "") _IK = uusIK;
        }

        public string IK
        {
            get => _IK;
            // set { if (_IK == "") _IK = value; }
            set => _IK = _IK == "" ? value : _IK;
        }

        public string Nimi
        {
            get => _Nimi;
            set
            {
                if (value == null) _Nimi = "unknown";
                else
                {
                    string[] osad = value.ToLower().Split(' ');     
                    for (int i = 0; i < osad.Length; i++)
                    {
                        osad[i] = osad[i].Substring(0, 1).ToUpper() + osad[i].Substring(1); // teisendus suurteks algustähtedeks
                    }
                    _Nimi = String.Join(" ", osad);
                }

            }
        }
    }
}
