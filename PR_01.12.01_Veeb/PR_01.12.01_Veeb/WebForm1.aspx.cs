﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PR_01._12._01_Veeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            northwindEntities ne = new northwindEntities();
            GridView1.DataSource = ne
                .Employees.Select(x => new { x.FirstName, x.LastName })
                .ToList();
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Request.Params["Nimi"] == null)
            this.Label1.Text = $"Tere, {this.TextBox1.Text}";
            else
            this.Label1.Text = $"Tere, {Request.Params["nimi"]}";
        }
    }
}