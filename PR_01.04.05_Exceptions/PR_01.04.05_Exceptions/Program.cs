﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._04._05_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kirjuta kaks arvu:");
        
            try
            {
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine(a / b);
            }

            catch (DivideByZeroException e)
            {
                Console.WriteLine("Ära jaga nulliga!");
                throw;
            }

            catch (FormatException e)
            {
                Console.WriteLine("Vale formaat!");
            }

            catch (Exception e)
            {
                Console.WriteLine("Viga!");
                Console.WriteLine(e.GetType().Name);
                Console.WriteLine(e.Message);
            }   

            finally
            {
                Console.WriteLine("Paranda vead!");
            }
        }
    }

}
