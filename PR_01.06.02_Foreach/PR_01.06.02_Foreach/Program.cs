﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_cycles
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 2 + 1, 3 + 4, 4, 5, 6 + 4 };         // luuakse int tüüpi massiiv[], millel on 6 väärtustatud elementi
            int jrknr = 1;

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 - Foreach meetod");

            foreach (int x in arvud)
            {
                Console.WriteLine(jrknr++ + $".nr on {x}");         // kuvatakse iga rea väärtused; kui ühel real, siis võib ka ilma loogeliste sulgudeta
            }
        }
    }
}
