﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Project_1_dataTxtFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Nimed_vanus.txt";                  // viide andmefailile; võib välja kirjutada ka pikalt, kuid soovitav lühidalt - .. tähistab ülemkataloogide astmeid
            string[] loetudread = File.ReadAllLines(failinimi);           // defineeritakse string tüüpi massiiv[], mis võtab väärtused viidatud faili kõikidest ridadest

            //++++++++++++++++++++++++++
            Console.WriteLine("\nForeach meetod");                        // lisatakse tühi rida (\n) ja kuvatakse jutumärkide vahel olev tekst 

            foreach (string rida in loetudread)                           // massiiv käiakse rida-realt läbi Foreach meetodiga
            {
                Console.WriteLine(rida);                              // kuvatakse iga rea väärtused; kui ühel real, siis võib ka ilma loogeliste sulgudeta
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nFor meetod");

            for (int i = 0; i < loetudread.Length; i++)                   // massiiv käiakse rida-realt läbi For meetodiga (algne väärtus;kordade arv;intervall)
            {
                Console.WriteLine(loetudread[i]);                     // kuvatakse iga rea väärtused; kui ühel real, siis võib ka ilma loogeliste sulgudeta
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nVeergude eraldamine");

            string[] nimed = new string[loetudread.Length];               // luuakse uus massiiv, milles on sama arv string tüüpi elemente, kui palju on andmefailis ridu
            int[] vanused = new int[loetudread.Length];                   // luuakse uus massiiv, milles on sama arv int tüüpi elemente, kui palju on andmefailis ridu

            for (int i = 0; i < loetudread.Length; i++)
            {
                string rida = loetudread[i];                            // defineeritakse string tüüpi muutuja, millele omistatakse vastava rea väärtus massiivis
                string[] osad = rida.Split(',');                        // defineeritakse string tüüpi massiiv[], mis võtab väärtused vastavalt realt ning eraldab need etteantud koma (või muu) separaatoriga                                                                      
                nimed[i] = osad[0];                                     // defineeritakse muutujad, mis saavad väärtuse massiivi vastava rea esimesest veerust (lugemist alustatakse nullist)
                Console.WriteLine(nimed[i]);                            // kuvatakse iga rea esimese veeru väärtused
                vanused[i] = int.Parse(osad[1].Trim());                 // defineeritakse muutujad, mis saavad väärtuse massiivi vastava rea teisest veerust; lisatakse trim funktsioon, et kaotada võimalikud tühikud
                Console.WriteLine(vanused[i]);
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nArvutamine");

            double summa = 0;                                               // double tüüpi muutuja väärtustamine nulliga
            double summa2 = 0;
            foreach (int x in vanused)                                      // massiiv käiakse rida-realt läbi Foreach meetodiga
            {
                summa += x;                                                 // avaldis, kus summale lisatakse iga tsükliga järgmise rea väärtus juurde
                summa2 = summa2 + x;                                        // avaldis, kus summale lisatakse iga tsükliga järgmise rea väärtus juurde (VBA)
            }

            double keskmine = summa / vanused.Length;                       // avaldis, kus summast jagatakse "vanuse" massiivi ridade arv                       
            Console.WriteLine($"Vanuste summa on {summa}");                 // eeltoodud avaldiste tulemused kuvatakse välja; muutuja lisatakse teksti sisse loogeliste sulgude vahele ja jutumärkide ette panna $
            Console.WriteLine($"Vanuste summa on {summa2}");
            Console.WriteLine($"Keskmine vanus on {keskmine}");


        }
    }
}
