﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._08_class9
{
    class Program
    {
        static void Main(string[] args)
        {
            Lihtne rebane = new Lihtne { Nimi = "Rebane" };
            Lihtne jänes = new Lihtne { Nimi = "Jänes" };

            rebane.Ohoo();
            Lihtne.Ohoo(jänes);

            E.Ehee(jänes);
            rebane.Ehee();                                                          // viide klassile E puudub - käiakse läbi kõik static classi meetodid

            "mati läheb koju"                
                .Replace("koju","metsa") 
                .ToUpper()
                .Tryki();

            int[] palgad2 = { 1000, 2000, 3000, 3000, 300 };
            List<int> palgad = new List<int>{ 1000, 2000, 3000 };
            Console.WriteLine(palgad.Summa());

            int[] arvud = { 1,3,7,5,8,2 };

            foreach (var x in arvud
                .Paaris()
                ) Console.WriteLine(x);

            foreach (var x in arvud
                .Kas(E.KasPaaritu)
                ) Console.WriteLine(x);

            Func<int, bool> kj3 = x => x % 3 == 0;
            foreach (var x in arvud
                .Kas(kj3)
                .Kas( x => x < 5)
                ) Console.WriteLine($"Jagub kolmega {x}");

            foreach (var x in arvud
                .Võta(3)
                .Take(3)                                        // süsteemi funktsioon, mis annab samaväärse tulemuse, mis eelnev
                //.SkipWhile(x => x !=3)
                .Select(x=> x*x)
                ) Console.WriteLine($"{x}");

            Console.WriteLine(arvud
                .Where(x => x % 2 == 0)
                .DefaultIfEmpty()
                .Sum()
                );

            var teeblisti = arvud.Where(x => x % 2 == 0).ToList();  //või ToArray (massiiv)

            Func<int, int> mingiValem = (x) => x * x + x;
            Func<int, int, int> mingiValem2 = (x,y) => x * y + x;
            Console.WriteLine(mingiValem(2));

            foreach (var x in palgad
                .Select (x=> x<500 ? 0:(x-500)*1.2)
                ) Console.WriteLine(x);

            var q = from x in arvud
                    where x % 2 == 0
                    orderby x descending
                    select new { arv = x, ruut = x * x };
            foreach (var x in q) Console.WriteLine(x);

        }
    }

    class Lihtne
    {
        public string Nimi;
        public void Ohoo() => Console.WriteLine($"{this.Nimi} ütleb Ohoo");
        public static void Ohoo(Lihtne l) => Console.WriteLine($"{l.Nimi} ütleb Ohoo");  // samaväärne eelmisega
    }

    static class E                          // static classis saab hoida vaid static asju
    {
        public static void Ehee(this Lihtne l) => Console.WriteLine($"{l.Nimi} ütleb Ehee");

        public static void Tryki(this string sõna) => Console.WriteLine(sõna);

        public static int Summa(this IEnumerable <int> arvud)
        { int summa = 0;
            foreach (var x in arvud)
            summa += x;
            return summa;
        }

        public static IEnumerable<int> Paaris(this IEnumerable<int> arvud)
        {   foreach (var x in arvud)
                if (x % 2 == 0) yield return x;
        }

        public static bool KasPaaris(int arv) => arv % 2 == 0;
        public static bool KasPaaritu(int arv) => arv % 2 == 1;

        public static IEnumerable<int> Kas (this IEnumerable<int> arvud, Func<int, bool> f)
        {
            foreach (var x in arvud)
                if (f(x)) yield return x;
        }

        public static IEnumerable<int> Võta(this IEnumerable<int> arvud, int mitu)          // mitu arvu muutuja algusest võtta
        {
            int i = 0;
            foreach (var x in arvud)
            {
                if (++i > mitu) break;
                yield return x;
            }

        }

    }
}
