﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._02._04_dataDatabase3
{
    partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }

    class Program
    {
        static void Main(string[] args)
        {
            northwindEntities ne = new northwindEntities();
                ne.Products
                    .Where(x => x.UnitPrice < 50)
                    .Where(x => x.UnitPrice > 10)   
                    .Where(x => !x.Discontinued)
                    .Select(x => new { x.ProductName, x.UnitPrice })
                    .ToList()
                    .ForEach(x => Console.WriteLine($"Toode: {x.ProductName} \thinnaga {x.UnitPrice}"));
    
            foreach (var e in ne.Employees)
                Console.WriteLine($"{e.FullName} {e.Manager?.FullName ?? "puudub"}");  //? - ära täida, ?? - täida (coalesce)
        }
    }
}
