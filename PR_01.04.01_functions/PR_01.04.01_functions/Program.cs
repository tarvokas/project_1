﻿// FUNCTIONS

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;

namespace Project_1_functions
{
    class Program
    {
        static void Main(string[] args)
        {

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1");

            string nimi = "Malle Maasikas";
            string proov = nimi.Replace("Malle", "Ville");

            string proov2 = nimi
                .Replace("Malle", "Ville")
                .ToUpper()
                .Split(' ')
                [0]
                .Replace("E", "U")
                .Remove(3, 1)
                ;

            Console.WriteLine(proov);
            Console.WriteLine(proov2);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - ToTitleCase1");

            Console.WriteLine("Sisesta nimi?");
            String nimi2 = (Console.ReadLine());
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            Console.WriteLine(textInfo.ToTitleCase(nimi2));

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3 - ToTitleCase2");

            Console.WriteLine("Sisesta nimi?");
            String[] nimed = Console.ReadLine().ToLower().Split(' ');                         // luuakse massiiv eelnevalt sisestatud sõnadest, mis on eraldatud tühikuga; tähed muudetakse väikeseks
            //Console.WriteLine("Eesnimi on {0}", nimed[0]);

            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1);          // massiivi iga element väärtustatakse ümber selliselt, et elemendi esimene täht suurendatakse ning lisatakse sellele ülejäänud tähed
            }
            string õigenimi = string.Join(" ", nimed);                                          // massiivi elemendid liidetakse uuesti kokku, kusjuures separaatoriks on tühik
            Console.WriteLine(õigenimi);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4 - ToTitleCase3");

            õigenimi = "";
            foreach (var x in nimed)
                õigenimi += x.Substring(0, 1).ToUpper() + x.Substring(1) + "";
            Console.WriteLine(õigenimi.Trim());

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 5 - matem funktsioonid");

            double arv3;

            arv3= Math.Pow(2, 3);
            Console.WriteLine(arv3);
        }
    }
}
