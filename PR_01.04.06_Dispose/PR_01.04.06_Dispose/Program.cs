﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FInalizerDisposableUsing
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;

            //while (i++ < 1000000) { A a = new A();  a.Dispose(); }

            while (i++ < 1000000)
                using (A a = new A())
                {
                    System.Threading.Thread.Sleep(100);
                }
        }
    }

    class A : IDisposable
    {
        static int gnr;
        int nr = gnr++;
        int[] sisu = new int[100000000];

        public A()
        {
            Console.WriteLine($"luuakse a nr {nr}");
        }

        ~A()
        {
            Console.WriteLine($"objekt a nr {nr} visatakse mälust välja");
            Dispose(false);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~A() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
