﻿// Meetod ja funktsioon
/*Meetod on nimeline programmi lõik.
Funktsioon on meetod, mis tagastab mingi väärtuse.
Parameeter on funktsiooni või meetodi sisene muutuja, mis saab väärtuse (argumendi) väljakutsumise hetkel.
Static meetodid on klaasi küljes.
Mitte-static kutsutakse välja objekti küljest.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_üldine
{
    class Program
    {
        static void Main(string[] args)
        {
            int arv = 7;
            int arv1 = 1;
            string p = "ahaha";
            double arv2 = 2;
            double arv3;

            arv += 77;
            arv2 = arv2 + 77;
            arv3 = Math.Pow(2, 3);

            Console.WriteLine(arv + 1);
            Console.WriteLine(arv2);
            Console.WriteLine(arv3 + arv2);
            Kuva(Liida(2, 3));
            Kuva2(Liida(2, 3), p);                                  // Funktsiooni saab kasutada avaldises
            Kuva2(p, "ah");
            Kuva2(p, p);

            Console.WriteLine(Liida2("kaks arvu", 3, 4));

            Swap(ref arv, ref arv1);
        }

        static int Liida(int x, int y)                              // defineeritakse uus avaldisega funktsioon, mida saab kasutada antud klassi piires
        {
            return x + y + 100;
        }

        static int Liida2(string mida, params int[] arvud)         // defineeritakse uus avaldisega funktsioon, mida saab kasutada antud klassi piires
        {
            Console.WriteLine($"liidan {mida}");
            int summa = 0;
            foreach (var x in arvud) summa += x;
            return summa;

        }

        static void Kuva(object a)                                  // defineeritakse uus meetod, mida saab kasutada antud klassi piires
        {
            Console.WriteLine(a);
        }

        static void Kuva2(object a, string nimi)                  // defineeritakse uus meetod, mida saab kasutada antud klassi piires
        {
            Console.WriteLine($"{nimi}={a}");
        }

        static void Kuva3(string a, string nimi)                  // defineeritakse uus meetod, mida saab kasutada antud klassi piires
        {
            Console.WriteLine($"{nimi}={a}");
        }


        static void Swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }

        static void Swap2<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }

    }

}
