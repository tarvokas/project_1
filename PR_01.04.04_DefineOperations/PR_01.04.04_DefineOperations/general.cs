﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._04._04_DefineOperations
{
    class general
    {
        public string Nimi;
        public void Meetod1()
        {
            Console.WriteLine($"seda teeb 1.meetod { this.Nimi}");

        }

        public static void Meetod2()
        {
            Console.WriteLine("seda teeb 2.meetod");

        }
    }
}
