﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_test
{
    class Program
    {
        static void Main(string[] args)
        {



            /*
            int[] mass = new int[5];                                                // luuakse int tüüpi massiiv[], millel on 5 väärtustamata elementi
            int[] arvud = { 1, 6, 3, 4, 8, 6 };


            Console.WriteLine("\nHarjutus 4 - While meetod");

            int y = 0;                                                              // defineeritakse algväärtus
            while (arvud[y++] < 8)                                                  // tegevust sooritatakse seni, kuni massiivis olev esimene väärtus on väiksemad kui etteantud 5; sama mis - for (;mass[y++] < 5;)

            {
                int a = arvud[y--];
                Console.WriteLine($"{y}. {a}");                              // kuvatakse massiivi rea järjekoha number ja rea väärtus     
                Console.WriteLine(y);                                               // kuvatakse elemendi järjekoha number
                int b = arvud[y++];
            }


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 5 - Do meetod");

            int z = 0;                                                              // sarnane While meetodile, kuid järjestus algab nullist ja seega elemente ühe võrra rohkem
            do
            {
                Console.WriteLine($"{z + 1}. {arvud[z]}");
            }
            while (arvud[z++] < 5);
            */

            TimePeriod Aeg = new TimePeriod();
            Aeg.Hours = 8;
            Console.WriteLine(Aeg.Hours);

        }
    }

    class TimePeriod
    {
        private double _Hours;
        public double Hours
        {
            get => _Hours + 5; 
            set
            {
                  if (value < 0 || value > 10)
                  _Hours = 0;
                  else _Hours = 10;
   
            }
        }
    }

}
