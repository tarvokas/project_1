﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._04_class
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene kalle = Inimene.LooUus("Kalle", "111");
            kalle.Vanus = 10;
            //kalle.Ik ="111";                                   // Ik on read-only väli ja väärtuse saab anda läbi 'LooUus' meetodi parameetri 

            Inimene malle = Inimene.LooUus("Malle","222");
            malle.Nimi = "Pepe";                                 // nimi kirjutatakse üle

            Inimene ville = Inimene.LooUus("Ville","222");       // sama isikukoodiga andmeid ei lisata,
            ville.Vanus = 90;                                    // küll aga kirjutatakse üle isikukoodi "222" vanus

            Inimene.LooUus("Rulle", "333").Nimi = "Li";


            Inimene.LooUus("Sille", "555").Vanus = 5;

            // malle.UusVanus("222", 100);                      // ei tööta???
             
            foreach (var x in Inimene.Inimesed)       // dictionarist käiakse kõik read läbi
               Console.WriteLine(x.Ik + " " + x.Nimi +" " + x.Vanus);
            
        }
    }
}
