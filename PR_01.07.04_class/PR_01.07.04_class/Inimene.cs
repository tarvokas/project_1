﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._04_class
{
    class Inimene
    {
        static Dictionary<String, Inimene> _Inimesed  = new Dictionary<string, Inimene>();      // Luuakse dictionary, milles string ja Inimene tüüpi andmed
        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;  // IEn.. võimaldab kollektsiooni järjestikust lugemist

        public string Nimi="";                                      
        public readonly string Ik;                      // antud välja saab defineerida vaid läbi parameetri ja üle kirjutada ei saa
        public int Vanus;

        public Inimene(string nimi, string ik)          // konstruktor, mis käivitub uue inimese lisamisel; private puhul ei saa program'is käsitsi uut lisada, vaid tuleb kasutada meetodit LooUus
            {
            Ik = ik;                                      
            Nimi = nimi;
            _Inimesed.Add(ik, this);                    // dictionarisse lisatakse isikukood ja teiste väljade (nimi, vanus) väärtused 
            }

        public static Inimene LooUus(string nimi, string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : new Inimene(nimi, ik);  // luuakse 'Inimene' tüüpi meetod, kus atribuudiks on isikukood, ning millega kontrollitakse, kas dictionary juba sisaldab lisatavat isikukoodi ja vastasel juhul käivitatakse lisamise meetod           

        ~Inimene()                                      // destructor
        {
            Console.WriteLine("visatakse mälust välja");
        }
        /*public UusVanus(string ik, int uusVanus)      // ei tööta???
        {
            Ik = ik;
            UusVanus = Vanus > 50 ? uusVanus : Vanus;                   // Kui nimi tühi siis lisab uue nime
            _Inimesed.Add(ik, this);
        }
        */
    }
}
