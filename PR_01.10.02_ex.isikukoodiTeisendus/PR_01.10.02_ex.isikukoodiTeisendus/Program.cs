﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Project_1_ex2_isikukood
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\Nimed_isikukoodid.txt";
            //var sisu1 = File.ReadAllText(filename);     // read all tekst
            var loetudread = File.ReadLines(filename);       // read all lines

            DateTime dt = new DateTime();

            Dictionary<string, DateTime> nimekiri = new Dictionary<string, DateTime>();

            foreach (var rida in loetudread)
            {
                var osad = rida.Split(',');
                string ik = osad[1].Trim();
                string nimi = osad[0];

                // isikukoodi kuju    SYYMMDDxxxx
                //                    S - sugu ja sajand 1,3,5 - mehed, 2,4,6 naised
                //                                       1,2 - 1800 + midagi, 3,4 - 1900, 5,6 - 2000
                //                    YY aasta MM kuu DD päev



                nimekiri.Add(
                    nimi,
                    new DateTime(
                        (ik[0] - '1') / 2 * 100 + 1800 + int.Parse(ik.Substring(1, 2)),
                        int.Parse(ik.Substring(3, 2)),
                        int.Parse(ik.Substring(5, 2))
                        ))
                        ;

            }

            foreach (var x in nimekiri)
            {
                Console.WriteLine($"{x.Key} on sündinud {x.Value} ja ta on {(DateTime.Now - x.Value).Days} päeva siin elanud");
                int vanus = (DateTime.Now - x.Value).Days * 4 / 1461;
                DateTime sp = x.Value.AddYears(vanus + 1);
                Console.WriteLine($"ta on {vanus} aastane ja ta sünnipäev on {sp.ToShortDateString()}");

            }

            // umbes nii teeme seda järgmise nädala lõpus
            Console.WriteLine(
            nimekiri
                .Select(x => new { Nimi = x.Key, Sünnikuupäev = x.Value, Vanus = (DateTime.Now - x.Value).Days * 4 / 1461 })
                .Select(x => new { x.Nimi, x.Vanus, Sünnipäev = x.Sünnikuupäev.AddYears(x.Vanus + 1) })
                .OrderBy(x => x.Sünnipäev)
                .Select(x => $"{x.Nimi} sünnipäev on {x.Sünnipäev.ToShortDateString()} ja ta saab {x.Vanus + 1} aastaseks")
                .FirstOrDefault()

                );


        }





    }
}
