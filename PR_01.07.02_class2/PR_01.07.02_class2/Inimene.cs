﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._02_class2
{
    enum Gender { Naine = 0, Mees = 1 }

    class Inimene
    {
        public string Nimi;
        public string Isikukood;

        public DateTime Birthday()
        {
            string sajand = "";
            switch(this.Isikukood.Substring(0,1))
            {
                case "1": case "2": sajand = "18"; break;
                case "3": case "4": sajand = "19"; break;
                case "5": case "6": sajand = "20"; break;
            }

            string kp = sajand
                + Isikukood.Substring(1, 2) + "/"
                + Isikukood.Substring(3, 2) + "/"
                + Isikukood.Substring(5, 2);

            return DateTime.Parse(kp);
        }

        public int Age() => (DateTime.Today - this.Birthday()).Days * 4 / 1461;
      /*  {
            return (DateTime.Today - this.Birthday()).Days * 4 / 1461;
        }*/

        public Gender Gender()
        {
            return (Gender)(this.Isikukood[0] % 2);
        }

       public override string ToString()
        => ($"{Nimi} ja {Isikukood}");
    }
}
