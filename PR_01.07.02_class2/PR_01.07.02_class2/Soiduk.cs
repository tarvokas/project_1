﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._02_class2
{
    class Soiduk
    {
        public string Mark { get; set; }        // defineeritakse property väli
        public string Mudel;

        public int Rattaid;
        public int Vedavaid;
        public double Voimsus;
        public double Rattavoimsus()
            {
            return Voimsus / Vedavaid;
            }
    }
}
