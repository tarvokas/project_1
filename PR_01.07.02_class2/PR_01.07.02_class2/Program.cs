﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PR_01._07._02_class2
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Nimed_isikukoodid.txt";                  // viide andmefailile; võib välja kirjutada ka pikalt, kuid soovitav lühidalt - .. tähistab ülemkataloogide astmeid
            string[] loetudread = File.ReadAllLines(failinimi);                 // defineeritakse string tüüpi massiiv[], mis võtab väärtused viidatud faili kõikidest ridadest
            List<Inimene> nimekiri = new List<Inimene>();                       // luuakse uus list 'nimekiri' tüübiga 'Inimene', mille komponendid on defineeritud klassisi 'Inimene'    

            ////////////////////////////////////
            Console.WriteLine("\nPikem listi lisamise variant");
            foreach (var rida in loetudread)                                    // massiiv käiakse rida-realt läbi Foreach meetodiga
            {
                Inimene uus = new Inimene();                                    // igast reast tekitatakse uus muutujate komplekt 
                    uus.Nimi = rida.Split(',')[0];                              // muutujale antakse rea esimese komaga eristatud välja väärtus  
                    uus.Isikukood = rida.Split(',')[1];
                    nimekiri.Add(uus);                                          // muutujate väärtused lisatakse listi 'nimekiri'
            }
            
            ////////////////////////////////////
            Console.WriteLine("\nLühem listi lisamise variant");

            foreach (var rida2 in loetudread)                                    // massiiv käiakse rida-realt läbi Foreach meetodiga
            {
                Inimene uus2 = new Inimene
                {
                    Nimi = rida2.Split(',')[0],
                    Isikukood = rida2.Split(',')[1]
                };
                nimekiri.Add(uus2);                                             // 'uus2' asemel saab kirjutada ka 'uus2=..' avaldis 
            }


            ////////////////////////////////////
            Console.WriteLine("\nVäljatrükk listist");

            foreach (Inimene x in nimekiri)
            {
                Console.WriteLine(x);                                           // x väärtus kujuneb klassi 'Inimene' meetodist 'public override..'
                Console.WriteLine(string.Format($"{x.Nimi} isikukoodiga {x.Isikukood}"));
                Console.WriteLine($"Sünnipäev: {x.Birthday()}");
                Console.WriteLine($"Vanus: {x.Age()}");
                Console.WriteLine($"Sugu: {x.Gender()}");
            }


            ////////////////////////////////////
            Console.WriteLine("\nKeskmine vanus");

            double vanusSum = 0;
            foreach (var x in nimekiri)
            {
                vanusSum += x.Age();
            }
            Console.WriteLine($"Keskmine vanus on  { vanusSum /  nimekiri.Count}");
          
        }
    }
}
