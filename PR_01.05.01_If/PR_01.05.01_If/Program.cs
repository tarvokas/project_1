﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_if
{
    class Program
    {
        static void Main(string[] args)
        {

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1");

            Console.Write("Kui palju sa palka saad? ");
            int palk = int.Parse(Console.ReadLine());
            double maks = 0;

            if (palk > 1000) // semikoolonit ei pane. Võib ka loogeliste sulgudeta panna, kui üks lause, nt Console.WriteLine("Hea palk");
            {
                Console.WriteLine("Hea palk");
                maks = 0.2;
                Console.WriteLine($"Tulumaks on {palk}*{maks} = " + palk * maks);
            }
            else if (palk > 500)
            {
                Console.WriteLine("Rahuldav palk");
                maks = 0.15;
            }
            else if (palk > 200)
            {
                Console.WriteLine("Kehv palk");
                maks = 0.1;
            }
            else
            {
                Console.WriteLine("Väga kehv palk");
                maks = 0.05;
            }


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2");

            decimal palk2 = 1000;
            decimal tulumaks = (palk2 > 500) ? (palk2 - 500) * 0.2M : 0;
            Console.WriteLine(tulumaks);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3");

            string nimi1 = "Kalle";
            Console.WriteLine("Ütle üks nimi: ");
            string nimi2 = Console.ReadLine();
            Console.WriteLine(nimi1 == nimi2 ? "on samad" : "erinevad");
            Console.WriteLine(nimi1.ToLower() == nimi2.ToLower() ? "on samad" : "erinevad");


            //++++++++++++++++++++++++++
            Console.WriteLine("\nTINGIMUSLIK ARVUTUSJADA (elvis)");

            Console.Write("Kui palju sa palka saad? ");
            int palk3 = int.Parse(Console.ReadLine());
            string palgasuurus =
                (palk3 > 1000) ? "Hea palk" :
                (palk3 > 500) ? "Keskmine palk" :
                (palk3 > 200) ? "Kehv palk" :
                "Väga kehv palk";
            Console.WriteLine(palgasuurus);



            //++++++++++++++++++++++++++
            Console.WriteLine("\nIF MEETOD 2");

            Console.WriteLine("Sisesta nr: ");
            int arv = int.Parse(Console.ReadLine());
            Console.WriteLine("Arv on {0}", arv);
            Console.WriteLine($"Arv on {arv}");
            if (arv % 2 == 0)
            {
                Console.WriteLine("Paarisarv");
            }
            else
            {
                Console.WriteLine("Paaritu arv");
            }

            Console.WriteLine(
                arv % 2 == 0 ? "Paarisarv" : "Paaritu arv");



        }
    }
}
