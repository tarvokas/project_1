﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_random
{
    class Program
    {
        static void Main(string[] args)
        {
            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1");

            Random r = new Random();

            for (int i = 0; i < 10; i++)
            {
                int x = r.Next(10);
                Console.Write($" {x}");
            }


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - Segamata pakk");
            Random r2 = new Random();

            List<int> kaardid = Enumerable.Range(0, 52).ToList();
            for (int i = 0; i < kaardid.Count; i++)
            {
                Console.Write($"\t{kaardid[i] + 1} {(i % 4 == 3 ? "\n" : "")}");
            }

            Console.WriteLine("\nHarjutus 2 - Segatud pakk");

            List<int> segatudKaardid = new List<int>();
            while (kaardid.Count > 0)
            {
                int i = r2.Next(kaardid.Count);
                segatudKaardid.Add(kaardid[i]);
                kaardid.RemoveAt(i);
            }

            kaardid = segatudKaardid;
            for (int i = 0; i < kaardid.Count; i++)
            {
                Console.Write($"\t{kaardid[i] + 1} {(i % 4 == 3 ? "\n" : "")}");
            }
        }
    }
}
