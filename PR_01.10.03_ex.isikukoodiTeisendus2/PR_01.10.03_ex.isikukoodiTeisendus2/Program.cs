﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._10._03_ex.isikukoodiTeisendus2
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\Nimed_isikukoodid.txt";                  // viide andmefailile; võib välja kirjutada ka pikalt, kuid soovitav lühidalt - .. tähistab ülemkataloogide astmeid
            string[] loetudread = File.ReadAllLines(failinimi);                 // defineeritakse string tüüpi massiiv[], mis võtab väärtused viidatud faili kõikidest ridadest


        }
    }
}
