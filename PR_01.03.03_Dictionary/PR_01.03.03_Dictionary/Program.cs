﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            //SortedList
            //SortedDictionary
            Dictionary<string, int> nimekiri = new Dictionary<string, int>();   // Uue dictionary loomine
            nimekiri.Add("Kalle", 10);                                           // Uute elementide lisamine, kusjuures esimene atribuut on võtmeväli ehk ühine tunnus ning teine väärtus          
            nimekiri.Add("Malle", 15);
            nimekiri.Add("Ville", 40);
            nimekiri.Add("Pille", 5);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 -  Dictionarist välja väärtuse otsimine");

            Console.WriteLine(nimekiri["Malle"]);                               // Kuvatakse otsitava objekti väärtus

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - ");

            Console.WriteLine("Listis on järgmised nimed:");
            foreach (var x in nimekiri.Keys) Console.WriteLine($"\t{x}");       // Kuvatakse dictionary kõikide objektide võtmeväljad
            foreach (var y in nimekiri.Values) Console.WriteLine($"\t{y}");     // Kuvatakse dictionary kõikide objektide väärtuse väljad
            foreach (var i in nimekiri) Console.WriteLine($"\t{i}");            // Kuvatakse dictionary kõikide objektide väljad

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3 - nimevastavuse otsimine Dictionarist");

            Console.WriteLine("Keda otsid?");
            string nimi = Console.ReadLine();
            if (nimekiri.ContainsKey(nimi))
                Console.WriteLine(nimekiri[nimi]);
            else Console.WriteLine("Sellist nime pole");

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4 - objekti-ülesed funktsioonid");

            foreach (var x in nimekiri)
                Console.WriteLine($"{x.Key} on {x.Value} aastat vana");

            Console.WriteLine(nimekiri.Values.Average());       // Funktsioonid
            Console.WriteLine(nimekiri.Values.Max());
            Console.WriteLine(nimekiri.Values.Min());

        }
    }
}
