﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._10._04_ex.kool
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene malle = new Inimene("222")
            {
                Nimi = "Malle",
                Aine = "mata",
                Klass = "1A"
            };

            Inimene kalle = new Inimene("333")
            {
                Nimi = "Kalle",
                Klass = "2B"
            };

            kalle.Vanemad.Add(malle);
            malle.Lapsed.Add(kalle);
            Inimene ville = new Inimene("444")
            {
                Nimi = "Ville",
                Klass = "1A"
            };

            "Õpilased".ReadLines()
                .Select(x => x.Replace(", ","").Trim())
                .Where(x =>)
        }
    }

    static class E
    {
        public static string ToProper(this string nimi)
            => nimi.Split(' ')

                .Select(x => x.Substring(0, 1).ToUpper() + x.ToLower()
                .Join();

        public static string Join(this IEnumerable<string> nimed, string eraldaja = " ")
            => String.Join(eraldaja, nimed);

        public static string FileName(this string name) => $@"..\..\{name}.txt";

        public static IEnumerable<string> 
         
    }

    class Inimene
    {
        public static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public IEnumerable<Inimene> Inimesed => _Inimesed.Values;

        public string IK { get; private set }
        private string _Nimi;
        public string Nimi { get => _Nimi; set => _Nimi = value.ToProper(); }
        public string Aine { get; set; } = "";
        public string Klass { get; set; } = "";
        public string KasAdmin { get; private set; } = false;

        public List<Inimene> Lapsed = new List<Inimene>();
        public List<Inimene> Vanemad = new List<Inimene>();

        public Inimene(string ik)
        { IK = ik; }

        public bool KasÕpetaja => Aine != "";
        public bool KasÕpilane => KasÕpetaja && Klass != "";
        public bool KasLapsevanem => Lapsed.Count >0;

        public override string ToString()
        =>
            => (KasÕpetaja ? $"{Aine} õpetaja " + (Klass == "" ? "" : (${ Klass} klassi juhataja"")):
            => (KasÕpilane ? $"{Aine} õpilane " : "") + Nimi
            +( KasLapsevanem ? $" (Lapsed:
            return base.ToString();
        }
*/

    }

}
