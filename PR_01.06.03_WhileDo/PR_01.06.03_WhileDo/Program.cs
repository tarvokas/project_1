﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_cycles
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 2 + 1, 3 + 4, 4, 5, 6 + 4 };                         // luuakse int tüüpi massiiv[], millel on 6 väärtustatud elementi

            int x = 1;
            while (x < 10)
            {
                Console.WriteLine(x);
                x++;
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 5 - While meetod");

            int y = 0;                                                              // defineeritakse algväärtus
            while (arvud[y++] < 5)                                                  // tegevust sooritatakse seni, kuni massiivis olev esimene väärtus on väiksemad kui etteantud 5; sama mis - for (;mass[y++] < 5;)
            {
                Console.WriteLine($"{y}. {arvud[y]}");                              // kuvatakse massiivi rea järjekoha number ja rea väärtus     
            }


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 6 - Do meetod");

            int z = 0;                                                              // sarnane While meetodile, kuid järjestus algab nullist ja seega elemente ühe võrra rohkem
            do
            {
                Console.WriteLine($"{z + 1}. {arvud[z]}");
            }
            while (arvud[z++] < 5);


        }
    }
}
