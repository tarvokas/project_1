﻿// DATATYPES

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_datatypes
{
    class Program
    {
        static void Main(string[] args)
        {

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 - ToString");

            int s1 = 7;
            int s2 = 3;
            string seitse = s1.ToString();
            Console.WriteLine(seitse);
            Console.WriteLine((s1 / s2).ToString());


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - Parse");
            Console.WriteLine("Kui vana sa oled?");
            int vanus = int.Parse(Console.ReadLine());

            int teisendus;
            string vanus2 = Console.ReadLine().ToString();
            Console.WriteLine(vanus2);
            if (int.TryParse(vanus2, out teisendus))                // Meetodi TryParse väljund on kas true või false
                Console.WriteLine($"teisendatud {vanus} -> {teisendus}");
            else
                Console.WriteLine($"teisendus ebaõnnestus");

            DateTime kp = DateTime.Parse("2018/12/10");
            Console.WriteLine(kp);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3 - casting");           // casting - ainult arvude puhul, pikemat arvu saab castida lühemaks
            int pool = (int)(vanus / 2.0);
            Console.WriteLine(pool);
            Console.WriteLine((double)(vanus / 2.50));

            int i = 7;
            byte b = (byte)i;
            Console.WriteLine(b);


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4 - Convert");

            int a = 1734;
            string a2 = Convert.ToString(a);
            Console.WriteLine(Convert.ToString(100));
            double c = Convert.ToDouble(Console.ReadLine());
            string d = Convert.ToString(Console.ReadLine());
            // DateTime f = Convert.ToDateTime(Console.ReadLine());
            int h = Convert.ToInt32(Console.ReadLine());

            //short ss = short(ii);


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 5 - char");

            char ahaa = 'A';
            Console.WriteLine(ahaa);
            Console.WriteLine(ahaa - 'a');
            Console.WriteLine(++ahaa);


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 6 - GetType");
            var asi = 7;
            Console.WriteLine(asi);
            Console.WriteLine(asi.GetType().Name);

            var asi2 = 7D;
            Console.WriteLine(asi2);
            Console.WriteLine(asi2.GetType().Name);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 7 - TypeValue");

            Console.WriteLine(int.MaxValue);
            Console.WriteLine(double.MaxValue);
            Console.WriteLine(byte.MaxValue);
            Console.WriteLine(float.MaxValue);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 8 - Nullable");

            int? aa = new int?();                           // nullable tüübil võib väärtus puududa (=null)
            Console.WriteLine(aa.HasValue);                 // tulemus: False
            Console.WriteLine(aa.HasValue ? aa : 0  );      // kui väärtus puudub, siis annab väärtuseks 0
            Console.WriteLine(aa ?? 0);                     // samaväärne eelmisega; coalesce funktsioon    

        }
    }
}
