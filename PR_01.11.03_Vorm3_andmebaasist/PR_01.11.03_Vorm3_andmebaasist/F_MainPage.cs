﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._03_Vorm3_andmebaasist
{
    public partial class F_MainPage : Form
    {
        public F_MainPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            F_Employees F_Employees = new F_Employees();
            F_Employees.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            F_Products F_Products = new F_Products();
            F_Products.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            F_Categories F_Categories = new F_Categories();
            F_Categories.Show();
        }
    }
}
