﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._03_Vorm3_andmebaasist
{
    public partial class F_Categories : Form
    {
        public F_Categories()
        {
            InitializeComponent();
        }

        private void F_Categories_Load(object sender, EventArgs e)
        {
            {
                northwindEntities ne = new northwindEntities();
                this.dataGridView1.DataSource = ne
                .Categories
                .Select(x => new { x.CategoryID, x.CategoryName, x.Description })
                .ToList();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            F_Products tooted = new F_Products();
            try
            {
                if (int.TryParse(this.dataGridView1.SelectedRows[0].Cells[0].Value.ToString(),
                    out int cid))
                {
                    tooted.CategoryID = cid;
                }
            }
            catch { }
            tooted.Show();
        }
    }
}
