﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._03_Vorm3_andmebaasist
{
    public partial class F_Products : Form
    {
        public int CategoryID = 0;

        public F_Products()
        {
            InitializeComponent();
        }

        private void F_Products_Load(object sender, EventArgs e)
        {
            {
                northwindEntities ne = new northwindEntities();

                this.dataGridView1.DataSource = ne.Products
                .Where(x => x.CategoryID == CategoryID || CategoryID == 0)
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })
                .ToList();
            }
        }
    }
}
