﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._03_Vorm3_andmebaasist
{
    public partial class F_Employees : Form
    {
        public F_Employees()
        {
            InitializeComponent();
        }
             
        private void F_Employees_Load(object sender, EventArgs e)
        {
            northwindEntities ne = new northwindEntities();

            List<string> nimed = new List<string> { "*" };  // luuakse list, kus on vaikimisi ühe objektina ka tärn
            nimed.AddRange(ne.Employees
                .Where(x => x.Subordinates.Count > 0)
                .Select(x => x.LastName)
                .ToList());
            this.listBox1.DataSource = nimed;               // listboxile antakse listi objektid    
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string lastName = this.listBox1.SelectedItem.ToString();        // defineeritakse objekt, mis on valitud listboxi objektidest

            northwindEntities ne = new northwindEntities();

            this.dataGridView1.DataSource = ne.Employees
                .Where(x => x.Manager.LastName == lastName || lastName == "*")  //tabelisse päritavad andmed piiratakse ära listboxi valikuga või *-ga
                .Select(x => new { x.EmployeeID, x.FirstName, x.LastName, x.HireDate })  // luuakse massiiv ja teisendatakse listiks
                .ToList();
        }
    }
}
