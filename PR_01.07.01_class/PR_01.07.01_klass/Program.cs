﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._01_klass
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene muutuja1 = new Inimene();       // uue klassi 'Inimene' kuuluva muutuja väärtustamine - variant 1; 'new' käivitab konstruktori 'public Inimene..'
            muutuja1.Nimi = "Kalle";
            muutuja1.Vanus = 50;
            muutuja1.Kaal = 105;
            muutuja1.Pikkus = 1.63;

            Inimene muutuja2 = muutuja1;            // uue muutuja väärtustamine - variant 2
            muutuja2.Nimi = "Mati";

            Inimene muutuja3 = new Inimene          // uue muutuja väärtustamine - variant 3
            {
                Vanus = 30,
                Kaal = 75,
                Pikkus = 1.82
            };

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1");

            Console.WriteLine(muutuja1.Nimi);
            Console.WriteLine(muutuja1);            // käivitab 'public override' meetodi         
            Console.WriteLine(muutuja2);
            Console.WriteLine(muutuja3);

            muutuja1 = null;                        // muutuja väärtus nullitakse
            muutuja2 = new Inimene();

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2");

            Console.WriteLine(muutuja1);
            Console.WriteLine(muutuja2);
            Console.WriteLine(muutuja2.Vanus);
            Console.WriteLine(muutuja3.BMI());


        }
    }    
}
