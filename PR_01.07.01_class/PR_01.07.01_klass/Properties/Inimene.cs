﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._01_klass
{
    class Inimene
    {
        public string Nimi;                         
        public int Vanus;
        public int Kaal;
        public double Pikkus;
        public static string Isikukood;                         // käivitatakse klassi nime abil; antud juhul ei ole kasutatav

        public double BMI()                                     // kehakaaluindeks
        {
            return Kaal / (Pikkus * Pikkus);                    // muutujale BMI omistatakse avaldis 
        }

        public double BMI2() => Kaal / (Pikkus * Pikkus);       // samaväärne BMI-ga, kuid lühemal kujul

        public override string ToString()                       // kuvatakse, kui avaldises kasutada vaid tüübiga 'Inimene' muutujat    
            => ($"{Nimi} on {Vanus} aastat vana");

        List<Inimene> Inimesed = new List<Inimene>();           // luuakse uus list, kuhu hiljem lisatakse konstruktoriga järjekorranumbrid
        static int jooksevNumber = 0;
        private readonly int jrknr;

        public Inimene(string nimi = "nimeta inimene")          // konstruktor, mis käivitatakse uue objekti loomisel; atribuutites on muutuja 'nimi' nõutav, mis puudumise korral nimetatakse vaikimisi "nimeta inimene"
        {
            Nimi = nimi;                                      
            Inimesed.Add(this);
            this.jrknr = ++jooksevNumber;
            Console.WriteLine($"Klassi 'Inimene' lisatakse uus objekt nr.{jrknr}");
        }

        //public Inimene() : this("nimeta inimene")               // ei tööta??? teine võimalus nime puudumise teha samanimeline konstruktor, mis käivitatakse uue objekti loomisel, kui nime pole; järgmisena käivitab eeltoodud samanimelise protseduuri 
        //{}

    }
}
