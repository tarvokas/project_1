﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_ex1_keskmineVanus
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("\nHarjutus 1");
            // küsime mitu inimest meil on
            Console.Write("Mitu inimest on? ");
            int inimesi = int.Parse(Console.ReadLine());

            // teeme massiivi
            int[] vanused = new int[inimesi];

            // hakkame küsima
            for (int i = 0; i < inimesi; i++)
            {
                Console.WriteLine($"Inimene {i + 1}");
                //Console.Write("nimi: "); nimed[i] = Console.ReadLine();
                Console.Write("vanus: ");
                vanused[i] = int.Parse(Console.ReadLine());
            }

            // kontrolliks trükime välja

            for (int i = 0; i < inimesi; i++)
                Console.WriteLine($"inimene {vanused[i]} vanusega {vanused[i]}");

            // arvutame keskmise vanuse - kuidas
            double summa = 0;
            foreach (var v in vanused) summa += v;
            double keskmine = summa / inimesi;
            Console.WriteLine($"keskmine vanus on {keskmine}");



            Console.WriteLine("\nHarjutus 2");

            List<int> arvud = new List<int>();
            string rida = "*";
            while (rida != "")
            {
                Console.Write("Kirjuta vanus:");
                rida = Console.ReadLine();
                if (rida != "")
                {
                    arvud.Add(int.Parse(rida));
                }
            }

            if (arvud.Count > 0)
                Console.WriteLine(arvud.Average());
            else Console.WriteLine("Arvud puuduvad");


            Console.WriteLine("\nHarjutus 3");

            double summa3 = 0;
            int mitu = 0;
            do
            {
                Console.Write("Kirjuta vanus: ");
                rida = Console.ReadLine();
                if (rida != "")
                {
                    if (double.TryParse(rida, out double arv))
                    {
                        summa3 += arv;
                        mitu++;
                    }
                    else Console.Write("See pole arv!");
                }
            } while (rida != "");

            Console.WriteLine(summa3 / mitu);


        }
    }
}
