﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._07_class7
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom l = new Loom("krokodill");
            Loom l2 = new Loom();
            Console.WriteLine(l);                                                   // tekib funktsioonist public override
            Console.WriteLine(l2);                                                   // tekib funktsioonist public override
          //  l.TeeHäält();

   /*         Koduloom k = new Koduloom("Ämblik")
            {
                Nimi = "Aadu"
            };
            Console.WriteLine(k);

            Kass nurr = new Kass
            {
                Nimi = "Murri",
                Tõug = "siiam"
            };
            Console.WriteLine(nurr);

            Koer muki = new Koer
            {
                Nimi = "Muri",
            };
            Console.WriteLine(muki);

            nurr.Silita();
            nurr.TeeHäält();
            nurr.Sikutasabast();

            List<Loom> loomaaed = new List<Loom> {l, k, nurr};
            foreach (var x in loomaaed) Console.WriteLine(x);
            foreach (var x in loomaaed) x.TeeHäält();
 */  
        }
    }

    class Loom                                                                      //abstract class - Uut Looma objekti teha ei saa
    {
        public string Liik = "";                                                    // väli
        public string Liik2 { get; set; } = "";                                     // property

        public Loom(string liik = "tundmatu")=> Liik = liik;                         // konstruktor

      //  public override string ToString() => $"loom liigist {Liik}";                // funktsioon

        public virtual void TeeHäält() => Console.WriteLine($"{this} teeb koledat häält");      //'virtual' annab võimaluse mingis teises klassis seda ümber defineerima

    }

    /*
    class Koduloom : Loom
    {
        public string Nimi { get; set; } = "nimeta";                                     //Property

        public Koduloom(string liik) : base("liikipole") { }                         //Konstruktor
    }

    class Kass : Koduloom
    {
        public Kass() : base("kass") { }                                             //Konstruktor
        public string Tõug { get; set; } = "";                                     //Property

        private bool tuju = false;
        public void Silita() => tuju = true;
        public void Sikutasabast() => tuju = false;

        public override void TeeHäält()                                            // funktsioon
        {
            if (tuju)
                Console.WriteLine($"{Tõug} kass {Nimi}  lööb nurru");
            else
                Console.WriteLine($"{Tõug} kass {Nimi}  kräunub");
        }
        public override string ToString() => $"{Tõug} kiisu {Nimi}";

    }

    class Koer : Koduloom, ISöödav
    {
        public Koer() : base("koer") { }                                             //Konstruktor
        public string Bread { get; set; } = "krants";                                     //Property

        public override void TeeHäält()                                            // funktsioon
        {
            Console.WriteLine($"Koer {Nimi} haugub");
        }

        public void süüakse()                                            // funktsioon
        {
            Console.WriteLine($"Koer {Nimi} pistetakse nahka");
        }


        public override string ToString() => $"Koer {Nimi}";
    }


    interface ISöödav                   // abstraktne klass
    {
        void Süüakse();
    }


    class Sepik : ISöödav
    {
        public void süüakse();
        Console.WriteLine("keegi nosib sepikut");
    }
      
    */

}


