﻿// LIST - LOETELU
/* Loetelu (List) on objektide dünaamilise mahuga kogum. .NET raamistiku klass ArrayList
realiseerib üldist IList liidest. Selle kasutamisel ei tarvitse muutujaklassi määratleda – kõik liikmed
teisendatakse üldisesse klassi object. Rakendus töötab aga kiiremini ja muutuja tüübi erinevusest
tingitud tõrgete risk liikmete kasutamisel on väiksem, kui loetelu liikmed on ühte tüüpi ja tüüp
määratakse loetelu deklareerimisel. Selleks on uuemates .NET raamistiku versioonides nimeruum
System.Collections.Generic ja selles loetelu List, mis deklareeritakse koos liikmete tüübiga. 

Kuna loetelu elemendid on indekseeritud, siis saab loetelu liikmetega manipuleerimiseks
kasutada nii for kui ka foreach tüüpi korduseid*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Project_1_list
{
    class Program
    {
        static void Main(string[] args)
        {

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 - listi loomine ja väärtuste lisamine");

            List<int> arvud = new List<int>();          // uue int tüüpi listi loomine
            arvud.Add(7);                               // väärtuste lisamine listi
            arvud.Add(4);
            arvud.Add(3);
            arvud.Add(2);
            arvud.Add(8);
            arvud.Add(5);
            arvud.Add(9);
            arvud.Remove(3);

            Console.WriteLine(arvud[0]);

            for (int i = 0; i < arvud.Count; i++)
            {
                Console.WriteLine(arvud[i]);            //Iga listi elemendi kuvamine
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - massiivi ja listi loomine, andmete vahetamine ja iga elemendi kuvamine");

            int[] mass = { 1, 2, 3, 4, 5 };
            List<int> arvud2 = mass.ToList();

            for (int i = 0; i < arvud2.Count; i++)
            {
                Console.WriteLine(arvud2[i]);
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3");

            Queue<int> järjekord = new Queue<int>();
            järjekord.Enqueue(1);
            järjekord.Enqueue(2);
            järjekord.Enqueue(7);

            while (järjekord.Count > 0) ;
            Console.WriteLine(järjekord.Dequeue());


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4");
            Queue<int> järjekord2 = new Queue<int>();
            järjekord2.Enqueue(1);
            järjekord2.Enqueue(2);
            järjekord2.Enqueue(7);

            while (järjekord2.Count > 0) ;
            Console.WriteLine(järjekord2.Dequeue());

            string failinimi = @"..\..\Nimed_isikukoodid.txt";                  // viide andmefailile; võib välja kirjutada ka pikalt, kuid soovitav lühidalt - .. tähistab ülemkataloogide astmeid
            string[] loetudread = File.ReadAllLines(failinimi);                 // defineeritakse string tüüpi massiiv[], mis võtab väärtused viidatud faili kõikidest ridadest
            List<string> nimekiri = new List<string>();                       // luuakse uus list 'nimekiri' tüübiga 'Inimene', mille komponendid on defineeritud klassisi 'Inimene'    



        }
    }
}
