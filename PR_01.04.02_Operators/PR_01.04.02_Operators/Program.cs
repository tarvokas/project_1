﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_10._04._02_Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus1 - Põhimärgid");

            int a1 = 15;                                    // omistamine
            int a2 = 10;
                                  
            Console.WriteLine(a1 + a2);                     // arvude summeerimine või teksti ühendamine
            Console.WriteLine(a1 += a2);                    // arvude summerimine ja esimese muutuja (a1) uus väärtustamine
            Console.WriteLine(a1 - a2);                     // arvude lahutamine
            Console.WriteLine(a1 -= a2);                    // arvude lahutamine ja esimese muutuja (a1) uus väärtustamine
            Console.WriteLine(a1 * a2);                     // arvude korrutamine
            Console.WriteLine(a1 *= a2);                    // arvude korrutamine ja esimese muutuja (a1) uus väärtustamine
            Console.WriteLine(a1 / a2);                     // arvude korrutamine
            Console.WriteLine(a1 /= a2);                    // arvude korrutamine ja esimese muutuja (a1) uus väärtustamine
            Console.WriteLine(20 % 6);                      // arvude jagamise jääk; nt 20 / 6 = int(3); 3 * 6 = 18; 20 - 18 = 2


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus2 - Lisamärgid");

            double a3 = 1.5;
            int a4 = 10;

            Console.WriteLine(a3++);                        // muutuja väärtustamine +1 võrra
            Console.WriteLine(a3);                          
            Console.WriteLine(++a4);                        // muutuja suurendamine ja väärtustamine +1 võrra
            Console.WriteLine(a4);

            Console.WriteLine(a3--);                        // muutuja väärtustamine -1 võrra
            Console.WriteLine(a3);
            Console.WriteLine(--a4);                        // muutuja vähendamine ja väärtustamine -1 võrra
            Console.WriteLine(a4);

            Console.WriteLine(!true);                       // vastandväärtuse omistamine

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus1 - Avaldised tekstis");

            string nimi = "Mati Karu";
            string eesnimi = "Mati";
            string perenimi = "Karu";
            int vanus = 30;

            string nimi2 = eesnimi + " " + perenimi;
            Boolean b = nimi == nimi2;
            Boolean b2 = nimi.Equals(nimi2);
            Console.WriteLine(b);
            Console.WriteLine(b2);

            string vastus = nimi + " on " + vanus.ToString() + " aastat vana";      // muutujad liidetakse ühte lausesse koos lisatekstiga; int tüübiga vanus muudetakse tüübiks string
            string vastus1 = $"{nimi} on varsti {vanus + 1} aastat vana";           // muutujad liidetakse ühte lausesse koos lisatekstiga; muutujad pannakse loogeliste sulgude vahele ja jutumärkide ette pannakse $
            Console.WriteLine(vastus);
            Console.WriteLine(vastus1);

            string vastus2 = string.Format("{0} on {1} aastat vana", nimi, vanus);
            Console.WriteLine(vastus2);

            Console.WriteLine("{0} on {1} aastat vana", nimi, vanus);

            Console.WriteLine($"{nimi} on varsti {vanus + 1} aastat vana");

            Console.WriteLine("\tÜtles: \"plaplapla\" \n ja läks mimema");          // \t - taandrida; \n - uus rida; \ enne jutumärke või kaldkriipsu ennast võimaldab neid ka tekstis kuvada

            Console.WriteLine("jutumärgid (\") tuleb katta kaldkriipsuga (\\) ");

            string tekst = @"C:\plaplapla\fail";                                    // @ jutumärkide ees käsitleb tekstis olevat kaldkriipsu tavalise sümbolina
            Console.WriteLine(tekst);

                                          
            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4 - Võrdlus");

            int aa1 = 1;
            int aa2 = 1;
            int aa3 = 3;
            
            Console.WriteLine(aa1 == aa2);
            Console.WriteLine(aa2 == aa3);
            Console.WriteLine(aa2 != aa3);

                // T1 & T2                                          // mõlemad tingimused on täidetud
                // T1 && T2                                         //  kui esimene tingimus ei ole täidetud, siis järgnevaid ei kontrollita.
                // T1 | T2                                          //üks tingimustest on täidetud
                // T1 || T2                                         // kui esimene tingimus on täidetud, siis järgnevaid ei kontrollita.
        }
    }
}
