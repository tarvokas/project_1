﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._01_Vorm1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Console.Beep(440, 1000);
            this.label5.Text = "Ära siis virise";
            this.button4.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.label5.Text = "Tõstame siis palka";
            this.button3.Visible = false;
        }
    }
}
