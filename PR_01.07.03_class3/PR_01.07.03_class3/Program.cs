﻿// Meetod ja funktsioon
/*Meetod on nimeline programmi lõik.
Funktsioon on meetod, mis tagastab mingi väärtuse.
Parameeter on funktsiooni või meetodi sisene muutuja, mis saab väärtuse (argumendi) väljakutsumise hetkel.
Static meetodid on klaasi küljes.
Mitte-static kutsutakse välja objekti küljest.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_üldine
{
    class Program
    {
        static void Main(string[] args)
        {
            int arv = 7;
            int arv1 = 5;
            string p = "ahaha";
            
            /////////////////////////////////////
            Console.WriteLine("\nHarjutus 1 - Liida");

            Console.WriteLine(Liida(arv, arv1));
            Kuva(Liida(arv, arv1));
            Liida(arv, arv1);

            
            /////////////////////////////////////
            Console.WriteLine("\nHarjutus 2 - Liida2");
            Console.WriteLine(Liida2(p, arv, arv1, 5));
            Console.WriteLine(Liida2("kolm arvu", 3, 4, 7));

            /////////////////////////////////////
            Console.WriteLine("\nHarjutus 3 - Liida2 massiivist");

            int[] mitu = { 1, 2, 3, 4, 5, 6, 7 };
            Console.WriteLine(Liida2("palju arve", mitu));

            /////////////////////////////////////
            Console.WriteLine("\nHarjutus 4 - Kuva2");
            Kuva(p + arv);
            Kuva2(Liida(arv, arv1), p);                                  // Funktsiooni saab kasutada avaldises
            Kuva2(p, "ah");
            Kuva2(p, p);


            /////////////////////////////////////
            Console.WriteLine("\nHarjutus 5 - Swap");
            Swap(ref arv, ref arv1);
        }


        static int Liida(int x, int y) =>  x + y + 100;                              // defineeritakse uus avaldisega funktsioon, mida saab kasutada antud klassi piires
        
            
        
        

        static int Liida2(string mida, params int[] arvud)         // defineeritakse uus avaldisega funktsioon, mida saab kasutada antud klassi piires; argumentidena on defineeritud string ja int tüüpi massiiv
        {
            Console.WriteLine($"liidan {mida}");
            int summa = 0;
            foreach (var x in arvud)
                summa += x;
            return summa;                                         // väljastab funktsiooni väärtusena summa
        }


        static void Kuva(object a) => Console.WriteLine(a);                                // defineeritakse uus meetod, mida saab kasutada antud klassi piires


        static void Kuva2(object a, string nimi)                  // defineeritakse uus meetod, mida saab kasutada antud klassi piires
        {
            Console.WriteLine($"{a} ja {nimi}");
        }


        static void Swap(ref int a, ref int b)
        {
            int t = a;
            a = b;
            b = t;
        }

        static void Swap2<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }

       
        
    }
}
