﻿// ENUMERATION (enum) - LOEND
/* Konstantide kogum, milles loendi liikmete väärtused määratakse loendi deklareerimisel. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_datatypes
{
    enum Mast { Risti, Ruutu, Ärtu, Poti }

    [Flags]
    enum Tunnus { Suur = 1, Väike = 2, Keskmine = 5 }

    class Program
    {
        static void Main(string[] args)
        {
            
            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1");

            Mast m = Mast.Ruutu;
            Console.WriteLine(m);
            Console.WriteLine((Mast)3);

            Console.WriteLine((Tunnus)5);
            Tunnus t = Tunnus.Suur | Tunnus.Väike;
            Console.WriteLine(t);

            
            //++++++++++++++++++++++++++
          //  Console.WriteLine("\nHarjutus 2");

          //  enum range { Min = 0, Max = 100 };
          //  float currentMinimum = (float)range.Min;
    }
    }
}
