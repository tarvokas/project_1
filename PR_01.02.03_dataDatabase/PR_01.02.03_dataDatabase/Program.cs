﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._02._03_dataDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection connection = new SqlConnection("Data Source=valiitsql.database.windows.net;Initial Catalog=northwind;User ID=Student;Password=Pa$$w0rd"))
            {
                connection.Open();

                SqlCommand comm = new SqlCommand("select productid, productname, unitprice from products", connection);

                SqlDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine($"Toode {reader.GetValue(1)} (koodiga {reader.GetValue(0)}) maksab {reader.GetValue(2)} ");
                }
            } // translaator pistab siia connction.Dispose()
        }
    }
}
