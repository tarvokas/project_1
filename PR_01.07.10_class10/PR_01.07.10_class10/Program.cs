﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._10_class10
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\spordipäevaprotokoll.txt";
            var protokoll = File.ReadAllLines(filename) //tulemusena on mul massiiv, mis koosneb ridadest. readalllines annab vastuseks strigni massiivi
                .Skip(1) // skipime esimese rea ja nüüd on meil massiivi asemel IEnumerable. skip annab vastuseks Ienumerable stringi. Skipile annad enne ienumerablei (nt massiv, list vms) ja nüüb vastu saad
                         //ienumerabli, mitte enam list v massivi vms. ienumberable on vist mingi asi, mida saab foreachiga läbi käia!

                 .Where(x => x.Trim() != "") //jätab kõik tühjad read vahele
                 .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())//sellest reast tulevad stringid, mis ta poolitab ära koma järgi. siis selectiga trimmin stringi ienumberabliks, kus on kõik massiivi elemdnid trimmitud. 
                                                                           //Slpit teeb stringist strngi massiivid. ToArrayga läheb tagasi massiviiks,
                 .Select(x => new { Nimi = x[0], Distants = int.Parse(x[1]), Aeg = double.Parse(x[2]) }) //see konstruktsioon teeb anonümset tüüpi struktuurist väärtustega asja (et on nimi, distants ja aeg)
                 .Select(x => new { x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg }).ToList(); // igast anonüümsest struktuurist teeb uue anonüümnse struktuuri. sisse tuleb struktuur, mis koosneb nimest distantsist ja ajast. 
                 
            protokoll.ForEach(x => Console.WriteLine(x)); //see rida on testimiseks
            
                        // kes on kõige kiirem jooksja

                        Console.WriteLine(protokoll
                            .OrderByDescending(x => x.Kiirus)
                            .Take(1)
                            .Select(x => $"\nkõigekiirem jooksja on {x.Nimi} tema kiirus on {x.Kiirus}")
                            .Single()
                            );


                        //kes on kõige kiirem spritner
                        Console.WriteLine(protokoll
                            .Where(x => x.Distants == 100)
                            .OrderByDescending(x => x.Kiirus)
                            .Take(1)
                            .Select(x => $"\nkõige kiirem sprinter on {x.Nimi} tema kiirus {x.Distants} meetrit")
                            .Single()
                            );


                        //kes on kõige lühema maa kiireim (kui me ei tea mis distants oli lühim)
                        var minDistants = protokoll.Min(y => y.Distants);
                        Console.WriteLine(protokoll
                           .Where(x => x.Distants == minDistants)
                           .OrderByDescending(x => x.Kiirus)
                           .Take(1)
                           .Select(x => $"\nkõige kiirem sprinter on {x.Nimi} tema kiirus {x.Distants} meetrit")
                           .Single()
                           );


            var stab = protokoll
                .ToLookup(x => x.Nimi)
                .Select(x => new { Nimi = x.Key, Mitu = x.Count(), Kiireim = x.Max(y => y.Kiirus), Aeglaseim = x.Min(y => y.Kiirus) }) // iga nime taha arvutatakse tema jooksude arv, kiireim ja aeglaseim aeg
                .Where(x => x.Mitu > 1)  // jjokse peab olema tehtud rohkem kui üks
                .OrderBy(x => Math.Abs(x.Kiireim - x.Aeglaseim))
                .FirstOrDefault();

            if (stab != null)
                Console.WriteLine($"Kõige stabiilseim oli {stab.Nimi}, kelle suurim kiirus oli {stab.Kiireim} ja väikseim aeg {stab.Aeglaseim}");

        }
    }
}
