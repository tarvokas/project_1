﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIsOnEvent
{
    class Program
    {
        static void SaadaPensionile(Inimene x) => Console.WriteLine($"{x.Nimi}, ahh mine pensionile!");

        static void Main(string[] args)
        {
            Inimene henn = new Inimene { Nimi = "Henn", Vanus = 64 };

            henn.Juubel += (Inimene x) => Console.WriteLine($"Palju õnne {x.Nimi} juubeli {x.Vanus} puhul");
            henn.Juubel += (Inimene x) => Console.WriteLine($"{x.Nimi} too torti ka!");

            henn.Pension += SaadaPensionile;





            for (int i = 0; i < 40; i++)
            {
                henn.Vanus++;
                Console.WriteLine($"Henn sai täna {henn.Vanus} aastaseks");

                if (henn.Vanus > 70) henn.Pension -= SaadaPensionile;

            }

        }
    }



    class Inimene
    {
        public event Action<Inimene> Juubel = null;
        public event Action<Inimene> Pension = null;

        public string Nimi { get; set; }
        private int _Vanus;
        public int Vanus
        {
            get => _Vanus;
            set
            {
                _Vanus = value;

                // kaks eventi väljakutset - pikem ja lühem
                if (_Vanus % 25 == 0)
                    if (Juubel != null) Juubel(this);
                if (_Vanus > 65) Pension?.Invoke(this);

            }
        }



    }
}
