﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._07_class7
{

    class Inimene
    {

        private static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static IEnumerable<Inimene> Inimesed// või => _Inimesed.Values;
        {
            get => _Inimesed.Values;
        }

        public static Inimene GetByIk(string ik)
        {
            if (_Inimesed.ContainsKey(ik)) return _Inimesed[ik];                //või => _Inimesed.ContainsKey(ik) ? _inimesed[ik] : null;
            else return null;
        }


        public string Nimi { get; set; }
        public string IK { get; private set; }

        public Inimene(string ik, string nimi)
        {
            IK = ik; Nimi = nimi;
            _Inimesed.Add(ik, this);
        }

        public override string ToString()
        {
            return $"Inimene {Nimi} (IK:{IK})";
        }
    }

    class Õpetaja : Inimene
    {
        public string Aine { get; private set; } = "";
        public string Klass { get; set; } = "";

        public Õpetaja(string ik, string nimi, string aine) : base(ik, nimi) => Aine = aine;

        public override string ToString()
        {
            return $"{Aine} õpetaja {Nimi} (IK:{IK})";
        }
    }

    sealed class Õpilane : Inimene              // sealed - sellele klassile ei saa enam viidata
    {
        public string Klass { get; set; } = "";
            public Õpilane(string ik, string nimi, string klass) : base(ik, nimi) => Klass = klass;

        public override string ToString()
        {
            return $" {Klass} õpilane "+ base.ToString();
        }
        public void UusKlass
    }

    
}


