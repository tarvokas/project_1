﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_01._07._08_class8
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene i = new Inimene("37806280291", "Tarvo");
            Console.WriteLine(i);
            
            Õpetaja kalle = new Õpetaja("34343543322", "Kalle","matem");
            Console.WriteLine(kalle);

            Õpilane ville = new Õpilane("34342321232", "Ville", "matem", "1A");
            Console.WriteLine(ville);

            foreach (var x in Inimene.Inimesed.Values)
            {
                Console.WriteLine(x);
                Console.WriteLine(x.value);
            }

            Console.WriteLine(Inimene.GetByIk("1234321233"));

            ///////////////////////////////////////////
            Console.WriteLine("\nIsikukoodi otsimine kataloogist");
            Inimene i2=null;                                
            if ((i2=Inimene.GetByIk("33322244"))==null)     //omistamine ja võrdlemine
                i2 =new Inimene("34342321232", "Ville");
            
        }
    }
}
