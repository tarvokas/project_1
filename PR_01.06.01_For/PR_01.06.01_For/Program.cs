﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_cycles
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] mass = new int[5];                                                // luuakse int tüüpi massiiv[], millel on 5 väärtustamata elementi
            int[] arvud = { 1, 2 + 1, 3 + 4, 4, 5, 6 + 4 };                               // luuakse int tüüpi massiiv[], millel on 6 väärtustatud elementi

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 - For meetod 1");

            for (int i = 0; i < mass.Length; i++)                                   // massiiv käiakse rida-realt läbi nii mitu korda, kui palju on massiivis ridu; (algne väärtus;kordade arv;intervall)
            {
                mass[i] = i * i;                                                    // defineeritakse muutujad, mis saavad väärtuse avaldisest, kus vastava rea järjekorra number korrutatakse endaga (loendamine algab nullist)

                Console.WriteLine(mass[i]);                                         // kuvatakse välja eelnevalt defineeritud avaldise tulemus
                Console.WriteLine(i * i);                                           // kuvatakse sulgudes toodud avaldise tulemus              
                Console.WriteLine(mass[i]++);
                Console.WriteLine(mass[i++]);                                       // kuvatakse välja eelnevalt defineeritud avaldise tulemusele järgnev väärtus         
            }

            Console.WriteLine("\nHarjutus 2 - For meetod 2");

            string text = "";
            for (int i = 0; i < 10; i++)
            {
                text = "Line " + i.ToString();
                Console.WriteLine(text);
            }
            Console.WriteLine("Viimane rida on" + text);


            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3 - For meetod 3");

            int mitu;                                                               // defineeritakse int tüüpi muutuja
            string nimi;                                                            // defineeritakse string tüüpi muutuja

            Console.WriteLine("Sisesta nimi:");                                     // kuvatakse jutumärkide vahel olev tekst
            nimi = Console.ReadLine();                                              // defineeritakse muutuja, mille väärtus sisestatakse konsooli
            Console.WriteLine("Mitu korda:");
            mitu = int.Parse(Console.ReadLine());                                   // defineeritakse muutuja, mille väärtus sisestatakse konsooli; muudetakse int tüübiks

            for (int i = 0; i < mitu; i++)                                          // (algne väärtus;kordade arv;intervall)
            {
                Console.WriteLine($"{nimi}, helistan sulle {i + 1}. korda!");       // kuvatakse jutumärkides olev tekst; muutujad pannakse loogeliste sulgude vahele ja jutumärkide ette $;
            }


  

        }
    }
}
