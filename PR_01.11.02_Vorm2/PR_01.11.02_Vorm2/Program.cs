﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._02_Vorm2
{
    public class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }
        public int Kaal { get; set; }
    }


    static class Program
    {
       // public static string[] Nimed_massiiv = { "Kalle", "Malle", "Ville" };
       // public static List<string> Nimed_list = Nimed_massiiv.ToList();
       public static List<string> Nimed_list = new List<string> { "Kalle", "Malle", "Ville" };

        public static List<Inimene> Nimed_list2 = new List<Inimene>
        {
            new Inimene {Nimi = "Kalle", Vanus = 64, Kaal = 45  },
            new Inimene {Nimi = "Malle", Vanus = 40, Kaal = 43  },
            new Inimene {Nimi = "Ville", Vanus = 28, Kaal = 42  },

        };

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Arvutusmasin());
        }
    }
}
