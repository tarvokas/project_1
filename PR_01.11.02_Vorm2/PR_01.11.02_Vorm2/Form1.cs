﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PR_01._11._02_Vorm2
{
    public partial class Arvutusmasin : Form
    {
        static int loendur = 0;
        int number = loendur++;

        public Arvutusmasin()
        {
            InitializeComponent();
        }

        private void Arvutusmasin_Load(object sender, EventArgs e)
        {
            this.label3.Text = $"vorm nr. {this.number+1} / {loendur}";
            this.listBox1.DataSource = Program.Nimed_list;
            this.tulemus.Visible = false;
        }        

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.tulemus.Visible = true;
            if (this.arv1.Text == "" || this.arv2.Text == "")
                this.tulemus.Text = "Sisesta arvud!";
            else
            {                
                double arv1 = double.Parse(this.arv1.Text);
                double arv2 = double.Parse(this.arv2.Text);
                this.tulemus.Text = "Tulemus on " + Math.Round((arv1 * arv2), 4).ToString();
            };
                
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.tulemus.Visible = true;
            if (this.arv1.Text == "" || this.arv2.Text == "")
                this.tulemus.Text = "Sisesta arvud!";
            else
            {
                double arv1 = double.Parse(this.arv1.Text);
                double arv2 = double.Parse(this.arv2.Text);
                this.tulemus.Text = "Tulemus on " + Math.Round((arv1 / arv2), 4).ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.tulemus.Visible = true;
            if (this.arv1.Text == "" || this.arv2.Text == "")
                this.tulemus.Text = "Sisesta arvud!";
            else
            {
                double arv1 = double.Parse(this.arv1.Text);
                double arv2 = double.Parse(this.arv2.Text);
                this.tulemus.Text = "Tulemus on " + Math.Round((arv1 + arv2), 4).ToString();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.tulemus.Visible = true;
            if (this.arv1.Text == "" || this.arv2.Text == "")
                this.tulemus.Text = "Sisesta arvud!";
            else
            {
                double arv1 = double.Parse(this.arv1.Text);
                double arv2 = double.Parse(this.arv2.Text);
                this.tulemus.Text = "Tulemus on " + Math.Round((arv1 - arv2), 4).ToString();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            (new Arvutusmasin()).Show();  //ShowDialog
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Text = $"Kasutaja: {this.listBox1.SelectedItem}";
        }

        private void button6_Click_1(object sender, EventArgs e)  // tühjendab arvude väljad
        {
            this.arv1.Text =null;
            this.arv2.Text = null;
            this.tulemus.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Program.Nimed_list.Add(this.textBox1.Text);
            listBox1.DataSource = null;
            this.listBox1.DataSource = Program.Nimed_list;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Program.Nimed_list.Remove(this.listBox1.SelectedItem.ToString());
            listBox1.DataSource = null;
            this.listBox1.DataSource = Program.Nimed_list;
        }
    }
}
