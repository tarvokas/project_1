﻿// ARRAY - MASSIIV
/* Massiiv(Array) on kindla arvu ühte tüüpi muutuja väärtuste indekseeritud kogum.Massiivi
maht määratakse deklareerimisel ja selle muutmiseks tuleb massiiv uuesti moodustada.Massiivi
väärtused saab omistada deklareerimisel või hiljem, kuid erinevalt teistest kogumitest, ei saa
massiivi liikmeid dünaamiliselt eemaldada ega lisada. C# massiivid on nullipõhised ja
võivad olla mitmemõõtmelised.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1_massives
{
    class Program
    {
        static void Main(string[] args)
        {
            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 1 - arvu massiivide loomine ja elemendi esitlemine");

            int arv = 7;                                                    // int tüübiga muutujale antakse väärtus 7
            int[] arvud = new int[10];                                      // luuakse int tüübiga massiiv, millel on 10 elementi; vaikimisi väärtused on vastava tüübi omased (nt int puhul 0, stringi puhul tühi)
            int[] arvud1 = arvud;                                           // luuakse int tüübiga massiiv, mille elemendid võetakse eelmisest viidatud massiivist
            int[] arvud2 = { 1, 2, 3, 4, 5 * 5, 6, 7 };                     // luuakse int tüübiga massiiv, millel on 7 väärtustatud elementi; elemendi väärtustega saab tehteid teha
            int[] arvud3 = new int[7] { 1, 2 + arv, 3, 4, 5 * 5, 6, 7 };    // samaväärne eelmisega
            int[] arvud4;                                                   // väärtustatakse hilisemas avaldises, nt if tingimusega

            arvud4 = DateTime.Now.DayOfWeek == DayOfWeek.Friday ? arvud3 : arvud2;  //IF tingimuslause

            Console.WriteLine(arvud3[4]);                                  // kuvatakse massiivist vastava järjekohaga väärtus; lugemine algab nullist
            Console.WriteLine(arvud2.Length);

            foreach (var x in arvud3)                                      // käiakse läbi massiivi elemendid ja kuvatakse järjest 
            {
                Console.WriteLine(x);
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 2 - masti masiivi loomine ja elemendi esitlemine");

            string[] mastid = { "Poti", "Ärtu", "Ruutu", "Risti" };
            Console.WriteLine(mastid[0]);                                   // kuvatakse massiivi esimene element [0]; loendamine algab nullist

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 3 - arvu masiivi loomine ja elemendi esitlemine");

            int[] teisedArvud = Enumerable.Range(1, 1000).ToArray();
            Console.WriteLine(teisedArvud[100]);

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 4 - sisestatava nime tükeldamine ja esitlemine");

            Console.WriteLine("Mis su nimi on? ");
            String[] nimed = Console.ReadLine().Split(' ');
            Console.WriteLine("Sinu eesnimi on {0}", nimed[0]);
            if (nimed.Length > 1)
            {
                Console.WriteLine("Sinu perenimi on {0}", nimed[1]);
            }

            //++++++++++++++++++++++++++
            Console.WriteLine("\nHarjutus 5 - massiivi andmete lisamine listi");

            List<int> arvud_list = arvud2.ToList();                         // var1: luuakse list ja massiivi 'arvud2' andmed lisatakse listi

            List<int> arvud_list2 = new List<int>();
            arvud_list2.AddRange(arvud2);                                   // var2: samaväärne eelmisega, kuid teise funktsiooniga

            foreach (var x in arvud_list) Console.WriteLine(x);
        }
    }
}
